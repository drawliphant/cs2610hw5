from django.db import models

# Create your models here.
class Blog(models.Model):
	title=models.CharField(max_length=100)
	author=models.CharField(max_length=30)
	content=models.CharField(max_length=1000)
	post_date=models.DateTimeField('date posted')

class Comment(models.Model):
	blog=models.ForeignKey(Blog, on_delete=models.CASCADE)
	nickname=models.CharField(max_length=30)
	email=models.CharField(max_length=50)
	content=models.CharField(max_length=200)
	post_date=models.DateTimeField('date posted')