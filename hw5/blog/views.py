from django.shortcuts import render
from time import strftime

def post(request,blog_id):
	 return HttpResponse("You're looking at blog post %s." % blog_id)
	 
def comments(request, blog_id):
    response = "You're looking at the comments of blog post %s."
    return HttpResponse(response % blog_id)

def index(request):
	return render(request, 'blog/index.html',{'current_time': strftime('%c')})

def tech(request):
	return render(request, 'blog/tech.html',{'current_time': strftime('%c')})

def aboutme(request):
	return render(request, 'blog/aboutme.html',{'current_time': strftime('%c')})

def bribe(request):
	return render(request, 'blog/bribe.html',{'current_time': strftime('%c')})

def comment(request):

	return render(request, 'blog/comment.html',{'current_time': strftime('%c')})
def template(request):
	return render(request, 'blog/template.html',{'current_time': strftime('%c')})



