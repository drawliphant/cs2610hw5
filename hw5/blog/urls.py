"""hw5 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
	url(r'^index', views.index, name='index'),
	url(r'^tech', views.tech, name='tech'),
	url(r'^aboutme', views.aboutme, name='aboutme'),
	url(r'^bribe', views.bribe, name='bribe'),
	url(r'^template', views.template, name='template'),
	url(r'^comment',views.comment,name='comment'),
	path('<int:blog_id>/', views.post, name='post'),
    path('<int:blog_id>/comments/', views.comments, name='comments'),
] 